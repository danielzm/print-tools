/*global _, require, module*/
"use strict";

// -----------------------------------------------------------------------------------------------
// PDF Report Implementation
// -----------------------------------------------------------------------------------------------

//var PDFToolkit = require('./PDFToolkit');

var PDFReportGenerator = function PDFReportGenerator (options) {
  if (!options || !options.data || !options.config) {
    throw 'PDFReportGenerator needs options with data {object} and config {object} attributes.';
  }
  this.defaults = {
    table: {
      splitRowsIn2columns: false,
      marginBottom: 2,
      marginRight: 1
    }
  };
  this.data = options.data;
  this.config = _.extend(this.defaults, options.config);
  this.data.totalPages = this.getTotalPages();
  return this;
};
PDFReportGenerator.prototype.getTotalPages = function () {
  this.generate(); // this renders everything to calculate the total number of pages.
  return this.myPDFToolkit.pdf.internal.pages.length - 1;
};
PDFReportGenerator.prototype.generate = function () {
  this.myPDFToolkit = new PDFToolkit(this.config);
  var ReportGenerator = this;
  if (this.data.watermark) {
    this.myPDFToolkit.addPDFWatermark({
      image: this.data.watermark.image,
      format: this.data.watermark.format,
      width: 150,
      height: 150,
      x: 35,
      y: 60
    });
  }
  if (this.data.header) {
    this.myPDFToolkit.addPDFHeader(this.data.header);
  }
  if (this.data.footer) {
    this.myPDFToolkit.addPDFFooter({
      customText: this.data.footer.customText.label,
      pageCount: this.data.footer.pageCount,
      totalPages: this.data.totalPages || false
    });
  }
  var sections = this.data.sections,
    last = false;
  _.each(sections, function (section, index) {
    if (index === sections.length - 1) {
      last = true;
    }
    ReportGenerator.addReportSection(section, last);
  })
};

PDFReportGenerator.prototype.addReportSection = function (sectionData, last) {
  var ReportGenerator = this;
  var notConfigurableOptions = {
    renderBottomLine: (last) ? false : true
  };

  var section = new this.myPDFToolkit.SectionElement(_.extend({
    pdf: this.myPDFToolkit.pdf,
    parentInstance: this.myPDFToolkit,
    marginTop: 1,
    marginBottom: 2,
    marginLeft: 1,
    sectionTitle: (sectionData.title) ? sectionData.title.label : null,
    sectionSubtitle: (sectionData.subtitle) ? sectionData.subtitle.label : null
  }, sectionData, notConfigurableOptions));

  function cleanListItems (items) {
    return _.map(items, function (item) {
      return item.label;
    });
  }

  _.each(sectionData.elements, function(element, index, other) {
    var defaults = _.clone(ReportGenerator.defaults[element.elementType]) || {};

    if (element.elementType === 'table') {
      var tableConfig = _.extend(defaults, element, {
        rows: element.items,
        drawCell: function (data) {}
      });
      section.addTable(tableConfig);
    }  else if (element.elementType === 'list') {
      var listConfig = _.extend(defaults, element, {items: cleanListItems(element.items)});
      section.addList(listConfig);
    }  else if (element.elementType === 'text') {
      var textConfig = _.extend(defaults, element);
      section.addText(textConfig);
    }
  });

  section.renderEnd();
  return this;
};

PDFReportGenerator.prototype.getDataUriString = function () {
  return this.myPDFToolkit.pdf.output('datauristring');
};

//module.exports = PDFReportGenerator;