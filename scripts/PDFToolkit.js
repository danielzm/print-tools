/*global require, _*/
"use strict";

var PDFToolkit = function (options) {
  options = options || {};
  this.defaults = {
    'orientation': 'p',
    'units': 'mm',
    'format': 'letter', // letter size has height: 297mm, width: 210mm
    'fontFamily': 'helvetica',
    'marginTop': 10,
    'marginRight': 10,
    'marginBottom': 10,
    'marginLeft': 10
  };

  this.defaults.font = {
    size: {
      report: {
        title: 16,
        subtitle: 14
      },
      section: {
        title: 13,
        subtitle: 12,
        text: 9
      }
    }
  };

  this.config = _.extend(this.defaults, options);
  this.config.x = this.config.marginLeft;
  this.config.y = this.config.marginTop;

  this.pdf = new jsPDF(this.config.orientation, this.config.units, this.config.format);

  this.pdf.setFontType(this.config.fontFamily, 'normal');

  this.config.documentDimensions = this.getFullDocumentDimensionsInMM();
  this.config.innerDimensions = this.getInnerDocumentDimensionsInMM();
  this.config.PDFInternalPoints = this.PDFInternalPoints();
  this.config.contentArea = {
    y: {
      top: this.config.PDFInternalPoints.rightTop.y,
      bottom: this.config.PDFInternalPoints.rightBottom.y
    },
    x: {
      left: this.config.PDFInternalPoints.leftTop.x,
      right: this.config.PDFInternalPoints.rightTop.x
    }
  };
  this.sections = []; // Document Sections Collector

  return this;
};
PDFToolkit.prototype.getFullDocumentDimensionsInMM = function () {
  return this.pdf.internal.pageSize || {};
};
PDFToolkit.prototype.getInnerDocumentDimensionsInMM = function () {
  return {
    width: this.config.documentDimensions.width - (this.config.marginLeft + this.config.marginRight),
    height: this.config.documentDimensions.height - (this.config.marginTop + this.config.marginBottom)
  }
};
PDFToolkit.prototype.getDataUriString = function () {
  return this.pdf.output('datauristring');
};
PDFToolkit.prototype.addPDFHeader = function (headerData) {
  if (!headerData) {
    throw 'PDFToolkit.addPageHeader needs headerData {object}.';
  }

  this.PDFHeader = new this.PageHeader(_.extend(headerData, {parentInstance: this})).render();

  this.config.contentArea.y.top = this.PDFHeader.bottom();
  return this;
};
PDFToolkit.prototype.addPDFWatermark = function (data) {
  if (!data) {
    throw 'PDFToolkit.addWatermark needs data {object}.';
  }

  this.PDFWatermark = new this.WatermarkElement(_.extend(data, {parentInstance: this})).render();

  return this;
};
PDFToolkit.prototype.addPDFFooter = function (footerData) {
  if (!footerData) {
    throw 'PDFToolkit.addPDFFooter needs footerData {object}.';
  }

  this.PDFFooter = new this.PageFooter(_.extend(footerData, {parentInstance: this})).render();
  this.config.contentArea.y.bottom = this.PDFFooter.top();
  return this;
};
PDFToolkit.prototype.PDFInternalPoints = function () {
  return {
    leftTop: { x: this.config.x, y: this.config.y },
    rightTop: { x: this.config.x + this.config.innerDimensions.width, y: this.config.y },
    rightBottom: { x: this.config.x + this.config.innerDimensions.width, y: this.config.documentDimensions.height - this.config.marginBottom},
    leftBottom: { x: this.config.x, y: this.config.documentDimensions.height - this.config.marginBottom}
  }
};
PDFToolkit.prototype.addPage = function () {
  this.pdf.addPage(this.config.format, this.config.orientation);
  if (this.PDFHeader) {
    this.PDFHeader.render();
  }
  if (this.PDFWatermark) {
    this.PDFWatermark.render();
  }
  if (this.PDFFooter) {
    this.PDFFooter.render();
  }
};
PDFToolkit.prototype.countPages = function () {
  return this.pdf.internal.pages.length - 1;
};


// -----------------------------------------------------------------------------------------------
// Classes to Extend from
// -----------------------------------------------------------------------------------------------

// -------------------
// TextContent
// -------------------
PDFToolkit.prototype.TextContent = function (options) {
  if(!options || !options.pdf || !options.parentInstance) {
    throw 'TextContent needs options {object} parameter with ' +
    'pdf {pdf tool} , parentInstance {ReportPDFInstance} and text {string} attributes';
  }
  this.options = options;
  this.parentInstance = this.options.parentInstance;
  this.init();
  return this;
};
PDFToolkit.prototype.TextContent.prototype.init = function () {
  this.defaults = {
    fontSize: 14,
    maxWidth: 160,
    marginTop: 0,
    marginRight: 0,
    marginBottom: 0,
    marginLeft: 0,
    fontStyle: 'normal',
    textColor: 0,
    x: 0,
    y: 0,
    breakPagePoint: this.parentInstance.config.contentArea.y.bottom,
    breakPage: true
  };
  this.config = _.extend(this.defaults, this.options);

  this.text = this.options.text;

  this.innerX = this.config.x + this.config.marginLeft;
  if (this.config.horizontalAlign) {
    this.innerX = this.config.x + this.config.maxWidth - this.textWidth();
  }

  this.innerY = this.config.y + this.config.marginTop;
  return this;
};
PDFToolkit.prototype.TextContent.prototype.render = function () {
  this.config.pdf.setTextColor(this.config.textColor);
  this.config.pdf.setFontSize(this.config.fontSize);
  this.config.pdf.setFontStyle(this.config.fontStyle);

  if (this.config.breakPage && this.bottom() > this.config.breakPagePoint) {
    this.addPage();
    this.config.y = this.parentInstance.config.contentArea.y.top;
    this.innerY = this.config.y + this.config.marginTop;
  }

  this.textLines = this.config.pdf.splitTextToSize(this.text, this.config.maxWidth);
  this.config.pdf.text(this.innerX, this.innerY + this.lineHeight(), this.textLines);

  this.config.pdf.setTextColor(this.defaults.textColor);

  if (this.config.onRender) {
    this.config.onRender(this);
  }
  return this;
};
PDFToolkit.prototype.TextContent.prototype.addPage = function () {
  this.config.pdf.addPage(this.parentInstance.config.format, this.parentInstance.config.orientation);
  this.pageBreak = true;
  if (this.PDFWatermark) {
    this.PDFWatermark.render();
  }
  if (this.parentInstance.PDFHeader) {
    this.parentInstance.PDFHeader.render();
  }
  if (this.parentInstance.PDFFooter) {
    this.parentInstance.PDFFooter.render();
  }
};
PDFToolkit.prototype.TextContent.prototype.totalLines = function () {
  return this.config.pdf.splitTextToSize(this.text, this.config.maxWidth).length;
};
PDFToolkit.prototype.TextContent.prototype.lineHeight = function () {
  return this.config.fontSize * 0.352777778
};
PDFToolkit.prototype.TextContent.prototype.height = function () {
  return this.config.marginTop + (this.lineHeight() * this.totalLines()) + this.config.marginBottom;
};
PDFToolkit.prototype.TextContent.prototype.textWidth = function () {
  return this.parentInstance.pdf.getTextWidth(this.text);
};
PDFToolkit.prototype.TextContent.prototype.bottom = function () {
  return this.config.y + this.height();
};

// -------------------
// Table Element
// -------------------
PDFToolkit.prototype.TableElement = function (options) {
  if (!options.headers) {
    throw 'TableElement needs options object with headers {array} attibute.';
  }
  else if (!options.rows && !options.objectDataTable) {
    throw 'TableElement needs options.rows or options.objectDataTable';
  }
  this.parentInstance = options.parentInstance;
  this.options = options;
  this.init();
  return this;
};
PDFToolkit.prototype.TableElement.prototype.init = function(){
  this.defaults = {
    headers: [],
    rows: [],
    y: 10,
    x: 10,
    marginTop: 0,
    marginBottom: 0,
    marginLeft: 0,
    marginRight: 0,
    fontSize: 8,
    splitRowsIn2columns: false,
    pageBreak: true,
    onStartPage: function (data) {},
    drawHeaderRow: function drawHeaderRow (row, data, flags) { return row },
    drawCell: function drawCell (cell) { return cell },
    afterPageContent: function afterPageContent (data) {},
    onTableEnd: function (data) {}
  };
  var defaultStylesForAutoTable = {
    theme: 'grid',
    styles: {
      cellPadding: 1,
      rowHeight: 4
    },
    headerStyles: {
      textColor: 255,
      fillColor: [41, 128, 185]
    },
    margin: {
      top: 3,
      right: 3,
      bottom: 3,
      left: 10
    },
    startY: 10,
    overflow: 'linebreak',
    tableWidth: 'auto',
    page: function () {},
    pageStartPosition: this.parentInstance.config.contentArea.y.top,
    pageBreakPosition: this.parentInstance.config.contentArea.y.bottom
  };

  this.rows = this.processRows(this.options.rows);
  if(this.options.objectDataTable) {
    this.rows = this.mapObjectToTableRows(this.options.objectDataTable);
  }

  this.headers = this.options.headers;

  if (this.options.splitRowsIn2columns) {
    this.headers = this.headers.concat(_.clone(this.headers));
    this.rows = this.splitRowsIn2columns(this.rows);
  }
  this.config = _.extend(this.defaults, this.options); // styles for the auto table plugin
  this.stylesForAutoTable = _.extend(defaultStylesForAutoTable, {
    onStartPage: this.config.onStartPage,
    drawHeaderRow: this.config.drawHeaderRow,
    startY: this.config.y + this.config.marginTop,
    tableWidth: this.config.tableWidth,
    onTableEnd: this.config.onTableEnd,
    margin: {
      left: this.config.x + this.config.marginLeft,
      right: this.config.marginRight,
      top: this.config.marginTop,
      bottom: this.config.marginBottom
    },
    pageBreak: this.config.pageBreak,
    pageStartPosition: this.config.pageStartPosition,
    pageBreakPosition: this.config.pageBreakPosition
  });
  this.stylesForAutoTable.styles.fontSize = this.config.fontSize;

  // detect if a new page was added
  this.initialPages = this.config.pdf.internal.pages.length - 1;
};
PDFToolkit.prototype.TableElement.prototype.processRows = function(rows) {
  var TableElement = this;
  if(!rows) {
    return;
  }
  rows.forEach(function (row) {
    row.forEach(function (column) {
      column.label = TableElement.cleanLabel(column.label);
      column.indentSize = TableElement.getIndent(column.indent);
    });
  });
  return rows;
};
PDFToolkit.prototype.TableElement.prototype.getIndent = function(indent) {
  if (!indent || indent < 16) { return 0; }
  // indent is sent starting at 12 and every step is by 4, every level is traduced to 3mm
  // 12 = indent level 0
  // 16 = indent level 1
  return ((indent - 12) / 4) * 3;
};
PDFToolkit.prototype.TableElement.prototype.cleanLabel = function(label) {
  if(!label) {
    return '';
  }
  if (label && typeof label === 'string') {
    label = _.filter(label.split(/\s/), function(word) {
      if (word === "") {return false;}
      return true;
    }).join(' ');
  }

  return label;
};
PDFToolkit.prototype.TableElement.prototype.render = function() {
  try {
    this.table = this.config.pdf.autoTable(this.headers, this.rows, this.stylesForAutoTable);
  } catch (err) {
    console.error(err);
  }
  this.lastYPosition = this.table.autoTableEndPosY();
  // detect if a new page was added
  var actualPages = this.config.pdf.internal.pages.length - 1;
  if (actualPages > this.initialPages) {
    this.pageBreak = true;
  }

  return this;
};
PDFToolkit.prototype.TableElement.prototype.tableWidth = function() {
  return this.stylesForAutoTable.tableWidth;
};
PDFToolkit.prototype.TableElement.prototype.right = function() {
  return this.config.x + this.tableWidth() + this.stylesForAutoTable.margin.right;
};
PDFToolkit.prototype.TableElement.prototype.bottom = function() {
  return this.lastYPosition + this.stylesForAutoTable.margin.bottom;
};
PDFToolkit.prototype.TableElement.prototype.mapObjectToTableRows = function(object) {
  return _.map(object, function(value, key){
    return [key, value];
  });
};
PDFToolkit.prototype.TableElement.prototype.splitRowsIn2columns = function (tableRowsArray) {
  var middleIndex = Math.round(tableRowsArray.length / 2) - 1,
    twoColumnsArray = [];

  _.each(tableRowsArray, function(row, index){
    if(index > middleIndex) {
      var indexToJoin = ((index - 1) - middleIndex);
      twoColumnsArray[indexToJoin] = twoColumnsArray[indexToJoin].concat(row);
      return;
    }

    twoColumnsArray.push(row);
  });

  // in odd row numbers the last row will be incomplete
  if(tableRowsArray.length % 2) {
    var lastElement = twoColumnsArray.length - 1,
      blankColumn = {label: ''},
      columnsToInsert = [];

    _.each(_.range(0, twoColumnsArray[lastElement].length), function(){
      columnsToInsert.push(blankColumn);
    });
    twoColumnsArray[lastElement] = twoColumnsArray[lastElement].concat(columnsToInsert);
  }
  return twoColumnsArray;
};

PDFToolkit.prototype.ListElement = function (options) {
  if (!options || !options.items) {
    throw 'ListElement needs options object with items {array}.';
  }
  this.options = options;
  this.init();
  return this;
};
PDFToolkit.prototype.ListElement.prototype = Object.create(PDFToolkit.prototype.TextContent.prototype);
PDFToolkit.prototype.ListElement.prototype.init = function () {
  this.defaults = {
    bullets: true,
    marginLeft: 2,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 4,
    fontSize: 10,
    maxWidth: 160,
    fontStyle: 'normal',
    textColor: 0
  };

  this.config = _.extend(this.defaults, this.options);
  this.text = this.config.items;

  if (this.config.bullets) {
    this.text = this.addBulletsToItems(this.config.items);
  }

  this.parentInstance = this.config.parentInstance;
  this.innerX = this.config.x + this.config.marginLeft;
  this.innerY = this.config.y + this.config.marginTop;
  return this;
};
PDFToolkit.prototype.ListElement.prototype.addBulletsToItems = function (items) {
  return _.map(items, function (item) {
    return '- ' + item;
  });
};

// -------------------
// Section Element
// -------------------
PDFToolkit.prototype.SectionElement = function (options) {
  if (!options.parentInstance) {
    throw 'SectionElement needs parentInstance';
  }
  this.parentInstance = options.parentInstance;
  this.pdf = options.pdf;
  this.defaults = {
    marginTop: 1,
    marginBottom: 1,
    marginLeft: 0,
    marginRight: 0,
    renderBottomLine: true,
    fontColor: 0,
    fontSize: 12,
    fontStyle: 'normal'
  };

  this.config = _.extend(this.defaults, options);
  this.config = _.extend(this.config, this.getPositions());

  this.config.fullWidth = this.getSectionWidth();
  this.config.innerWidth = this.config.fullWidth - this.config.marginLeft - this.config.marginRight;
  this.config.width = {
    full: this.config.innerWidth,
    half: this.config.innerWidth / 2
  };

  this.elements = [];

  if (this.config.sectionTitle) {
    this.addTitle(this.config.sectionTitle);
  }
  if (this.config.sectionSubtitle) {
    this.addSubtitle(this.config.sectionSubtitle);
  }
  if (this.config.introParagraph) {
    this.addIntroParagraph(this.config.introParagraph);
  }

  this.parentInstance.sections.push(this);
  return this;
};
PDFToolkit.prototype.SectionElement.prototype.renderEnd = function () {
  if (this.config.renderBottomLine) {
    this.renderBottomLine();
  }
  return this;
};
PDFToolkit.prototype.SectionElement.prototype.renderBottomLine = function () {
  this.pdf.setDrawColor(0);
  this.pdf.line(this.config.x, this.bottom(), this.config.x + this.getSectionWidth(), this.bottom());
};
PDFToolkit.prototype.SectionElement.prototype.getPositions = function () {
  var x = this.parentInstance.config.x,
    y = this.calculateY();
  return {
    x: x,
    y: y,
    innerX: x + this.config.marginLeft,
    innerY: y + this.config.marginTop
  }
};
PDFToolkit.prototype.SectionElement.prototype.getSectionWidth = function () {
  return this.parentInstance.config.innerDimensions.width;
};
PDFToolkit.prototype.SectionElement.prototype.addTable = function(options) {
  if(!options || !options.width) {
    throw 'addTable needs options {object} and options.width {string} attributes.';
  }
  var parentInstance = this.parentInstance,
    renderHeaderAndFooter = false,
    defaultConfig  = {
      pdf: this.pdf,
      parentInstance: parentInstance,
      tableWidth: this.config.width.full,
      splitRowsIn2columns: false,
      marginBottom: 2,
      marginRight: 1,
      marginTop: 0,
      fontSize: parentInstance.config.font.size.section.text,
      pageStartPosition: this.parentInstance.config.contentArea.y.top,
      pageBreakPosition: this.parentInstance.config.contentArea.y.bottom,
      onTableEnd: function onTableEnd (data) {
        if (renderHeaderAndFooter) {
          parentInstance.PDFFooter.render();
          parentInstance.PDFHeader.render();
        }
      },
      // this makes the auto table plugin to continue rendering the table after the header
      drawHeaderRow: function drawHeaderRow (row, data, flags) {
        if(flags && flags.afterNewPage) {
          data.cursor.y = parentInstance.PDFHeader.bottom() + data.settings.margin.top + 1;
        }
        return true;
      }
    },
    positioning = this.getNewElementPositioning(options.width),
    tableConfig = _.extend(defaultConfig, options, positioning),
    table;

  tableConfig.tableWidth = this.config.width[options.width] - (this.config.marginLeft - this.config.marginRight);

  // break the page correctly
  tableConfig.onStartPage = function onStartPage (data) {
    renderHeaderAndFooter = true;
    parentInstance.PDFWatermark.render();
  };

  table = new parentInstance.TableElement(tableConfig);

  this.addElement(table);
  return this;
};
PDFToolkit.prototype.SectionElement.prototype.addList = function(options) {
  if(!options || !options.items) {
    throw 'addList needs options {object} with items {array} attribute.';
  }
  var defaultListConfig = {
      pdf: this.pdf,
      parentInstance: this.parentInstance,
      listWidth: 90
    },
    positioning = this.getNewElementPositioning(options.width),
    listConfig = _.extend(defaultListConfig, options, positioning, {maxWidth: this.config.width[options.width]}),
    list = new this.parentInstance.ListElement(listConfig);

  this.addElement(list);
  return this;
};
PDFToolkit.prototype.SectionElement.prototype.addText = function(options) {
  if(!options || !options.text) {
    throw 'addText needs options {object} with text {string} attribute.';
  }
  var section = this,
    defaultConfig = {
      pdf: this.pdf,
      parentInstance: this.parentInstance,
      maxWidth: this.config.width.full,
      fontSize: 10,
      marginBottom: 2
    },
    positioning = this.getNewElementPositioning(options.width),
    config = _.extend(defaultConfig, options, positioning, {
      maxWidth: this.config.width[options.width],
      onRender: function (textElement) {
        textElement.config.pdf.setTextColor(section.config.fontColor);
        textElement.config.pdf.setFontSize(section.config.fontSize);
        textElement.config.pdf.setFontStyle(section.config.fontStyle);
      }
    }),
    element = new this.parentInstance.TextContent(config);

  this.addElement(element);
  return this;
};
PDFToolkit.prototype.SectionElement.prototype.getNewElementPositioning = function(elementWidth){
  var width = { half: 95, full: 191};
  var previousElement = _.last(this.elements) || this.subtitle || this.title;

  var positioning = {};

  // side by side elements
  if(previousElement && previousElement.right && previousElement.right() < width.full && elementWidth === 'half') {
    positioning.x = previousElement.right();
    positioning.y = previousElement.config.y;
  } else {
    positioning.x = this.config.innerX;
    positioning.y = this.getLastYPosition();
  }
  return positioning;
};
PDFToolkit.prototype.SectionElement.prototype.addElement = function(element){
  if(!element) {
    throw 'addElement needs element {object}.'
  }
  this.elements.push(element);
  element.render();
};
PDFToolkit.prototype.SectionElement.prototype.calculateY = function () {
  if (this.parentInstance.sections.length > 0) {
    var previousSection = this.parentInstance.sections[this.parentInstance.sections.length-1];
    if (previousSection.bottom) {
      return previousSection.bottom();
    } else if (previousSection.defaultBottom) {
      return previousSection.defaultBottom();
    } else {
      throw 'You need to provide a bottom function to calculate the height of the section you created.'
    }
  }
  if (this.parentInstance.PDFHeader) {
    return this.parentInstance.PDFHeader.bottom();
  }

  return  this.parentInstance.config.marginTop;
};
PDFToolkit.prototype.SectionElement.prototype.addTitle = function (title) {
  if(!title) {
    throw 'addTitle needs title {string} parameter.'
  }

  this.title = new this.parentInstance.TextContent({
    pdf: this.pdf,
    parentInstance: this.parentInstance,
    x: this.config.innerX,
    y: this.config.innerY,
    text: title,
    fontSize: this.parentInstance.config.font.size.section.title,
    maxWidth: 200,
    marginBottom: 1
  }).render();
  return  this;
};
PDFToolkit.prototype.SectionElement.prototype.addSubtitle = function (text) {
  if(!text) {
    throw 'addSubtitle needs text {string} parameter.'
  }
  var yPosition = this.title ? this.title.bottom() : 0;

  this.subtitle = new this.parentInstance.TextContent({
    pdf: this.pdf,
    parentInstance: this.parentInstance,
    x: this.config.innerX,
    y: yPosition || this.config.innerY,
    text: text,
    fontSize: this.parentInstance.config.font.size.section.subtitle,
    maxWidth: 150,
    marginBottom: 1
  }).render();

  return  this;
};
PDFToolkit.prototype.SectionElement.prototype.addIntroParagraph = function (text) {
  if(!text) {
    throw 'addIntroParagraph requires text {string} parameter.';
  }
  var yPosition;
  if (this.subtitle){
    yPosition = this.subtitle.bottom();
  } else if(this.title) {
    yPosition = this.title.bottom();
  }

  this.introParagraph = new this.parentInstance.TextContent({
    pdf: this.parentInstance.pdf,
    parentInstance: this.parentInstance,
    text: text,
    x: this.config.innerX,
    y: yPosition || this.config.innerY,
    marginBottom: 3,
    fontSize: this.parentInstance.defaults.font.size.section.text
  }).render();
  return this;
};
PDFToolkit.prototype.SectionElement.prototype.defaultBottom = function () {
  var lastYPosition = this.config.y;
  if (this.introParagraph) {
    lastYPosition = this.introParagraph.bottom();
  } else if (this.subtitle && this.subtitle.bottom && this.subtitle.bottom()){
    lastYPosition = this.subtitle.bottom();
  } else if (this.title.bottom()){
    lastYPosition = this.title.bottom();
  }
  return lastYPosition + this.config.marginBottom;
};
PDFToolkit.prototype.SectionElement.prototype.getLastYPosition = function () {
  var lastYPosition = this.defaultBottom(),
    lastElement = _.last(this.elements),
    pageBreak = false;
  _.each(this.elements, function(element){
    if (element.pageBreak) {
      lastYPosition = lastElement.bottom();
    } else {
      var bottom = element.bottom();
      if (bottom > lastYPosition) {
        lastYPosition = bottom;
      }
    }
  });
  return lastYPosition;
};
PDFToolkit.prototype.SectionElement.prototype.bottom = function(){
  return this.getLastYPosition() + this.config.marginBottom;
};

// Page Header
PDFToolkit.prototype.PageHeader = function (options) {
  options = options || {};

  if (!options.image || !options.title || !options.parentInstance) {
    throw "PageHeader needs options {object} with logo {image} and title {string} and parentInstance {obejct}.";
  }
  this.parentInstance = options.parentInstance;
  var defaults = {
      marginTop: 0,
      marginBottom: 3,
      marginLeft: 0,
      marginRight: 0,
      renderBottomLine: true
    },
    notEditableConfig = {
      // the header will render from the start of the writable area in the page
      x: this.parentInstance.config.x,
      y: this.parentInstance.config.y,
    };
  this.elements = [];
  this.config = _.extend(defaults, options, notEditableConfig); // building config obj
  this.config = _.extend(this.config, this.getInnerXAndY()); // getting internal positions
  return this;
};
PDFToolkit.prototype.PageHeader.prototype.render = function () {
  var parentInstance = this.parentInstance;
  var logo, title, patientInfo;

  if (this.config.image) {
    logo = new parentInstance.LogoElement({
      pdf: parentInstance.pdf,
      parentInstance: parentInstance,
      x: this.config.innerX,
      y: this.config.innerY,
      image: this.config.image.data,
      imageFormat: this.config.image.format,
      marginRight: 1,
      marginBottom: 1
    });
    this.addElement(logo);
  }

  if (this.config.title) {
    var xPosition = this.config.innerX,
      yPosition =  this.config.innerY,
      maxWidth = 170;

    if (logo) {
      xPosition = logo.right();
      yPosition =  logo.config.y;
      maxWidth = 150;
    }
    var titleText = this.config.title.label;
    if (this.config.subtitle) {
      titleText += ' / ' + this.config.subtitle.label;
    }

    title = new parentInstance.TextContent({
      pdf: parentInstance.pdf,
      parentInstance: parentInstance,
      text: titleText,
      fontSize: parentInstance.config.font.size.report.title,
      maxWidth: maxWidth,
      marginTop: 0,
      marginBottom: 2,
      marginLeft: 1,
      x: xPosition,
      y: yPosition
    });
    this.addElement(title);
  }

  if (this.config.infoTable) {
    var xPosition = this.config.innerX,
      yPosition =  this.config.innerY,
      maxWidth = 170;

    if (title) { yPosition = title.bottom(); }
    if (logo) { xPosition =  logo.right(); }

    patientInfo = new parentInstance.TableElement({
      pdf: parentInstance.pdf,
      parentInstance: parentInstance,
      marginLeft: 1,
      tableWidth: 168,
      y: yPosition,
      x: xPosition,
      headers: this.config.infoTable.headers,
      pageBreak: 'avoid',
      rows: this.config.infoTable.items,
      splitRowsIn2columns: true
    });
    this.addElement(patientInfo)
  }

  //var subtitle = new myPDFToolkit.TextContent({
  //  pdf: myPDFToolkit.pdf,
  //  parentInstance: myPDFToolkit,
  //  fontSize: myPDFToolkit.config.font.size.report.subtitle,
  //  maxWidth: 150,
  //  text: this.config.subtitle.label,
  //  x: title.config.x,
  //  y: title.bottom(),
  //  marginLeft: title.config.marginLeft
  //});

  //this.addElement(subtitle);

  // adding this in the sections of the main Pdf Object
  if (this.config.renderBottomLine) {
    this.renderBottomLine();
  }
  return this;
};
PDFToolkit.prototype.PageHeader.prototype.addElement = function (element) {
  this.elements.push(element);
  element.render();
  return this;
};
PDFToolkit.prototype.PageHeader.prototype.getInnerXAndY = function () {
  return {
    innerX: this.config.x + this.config.marginLeft,
    innerY: this.config.y + this.config.marginTop
  };
};
PDFToolkit.prototype.PageHeader.prototype.getLastYPosition = function () {
  return _.max(this.elements, function(element){ return element.bottom(); }).bottom() || this.config.innerY;
};
PDFToolkit.prototype.PageHeader.prototype.bottom = function () {
  return this.getLastYPosition() + this.config.marginBottom;
};
PDFToolkit.prototype.PageHeader.prototype.renderBottomLine = function () {
  this.parentInstance.pdf.setDrawColor(0);
  this.parentInstance.pdf.line(this.config.x, this.bottom(), this.parentInstance.config.contentArea.x.right, this.bottom());
};

// -------------------
// Watermark Element
// -------------------
PDFToolkit.prototype.WatermarkElement = function(options) {
  this.defaults = {
    width: 100,
    height: 100,
    marginTop: 0,
    marginRight: 0,
    marginBottom: 0,
    marginLeft: 0
  };

  this.config = _.extend(this.defaults, options);
  this.config.pdf = options.parentInstance.pdf;

  return this;
};
PDFToolkit.prototype.WatermarkElement.prototype.render = function () {
  this.config.pdf.addImage(this.config.image, this.config.format,
    this.left(), this.top(), this.config.width, this.config.height);
  return this;
};

// -------------------
// Logo Element
// -------------------
PDFToolkit.prototype.LogoElement = function(options) {
  this.defaults = {
    width: 35,
    height: 15,
    marginTop: 0,
    marginRight: 0,
    marginBottom: 0,
    marginLeft: 0
  };

  this.config = _.extend(this.defaults, options);
  this.config.pdf = options.pdf;
  this.config.image = options.image;
  this.config.format = options.imageFormat;
  this.config.x = options.x;
  this.config.y = options.y;

  return this;
};
PDFToolkit.prototype.LogoElement.prototype.top = function () {
  return this.config.y + this.config.marginTop;
};
PDFToolkit.prototype.LogoElement.prototype.bottom = function () {
  return this.config.y + this.config.height + this.config.marginBottom;
};
PDFToolkit.prototype.LogoElement.prototype.left = function () {
  return this.config.x + this.config.marginLeft;
};
PDFToolkit.prototype.LogoElement.prototype.right = function () {
  return this.left() + this.config.width + this.config.marginRight;
};
PDFToolkit.prototype.LogoElement.prototype.render = function () {
  this.config.pdf.addImage(this.config.image, this.config.format,
    this.left(), this.top(), this.config.width, this.config.height);
  return this;
};

// Page Footer
PDFToolkit.prototype.PageFooter = function (options) {
  if (!options.config && !options.parentInstance) {
    throw "PageFooter needs options {object} with config {object} and parentInstance {object}.";
  }

  this.parentInstance = options.parentInstance;
  this.options = options;
  this.init();
  return this;
};
PDFToolkit.prototype.PageFooter.prototype.init = function () {
  var defaults = {
    marginTop: 2,
    marginBottom: 0,
    marginLeft: 0,
    marginRight: 0,
    fontSize: 10,
    textColor: 115,
    pageCount: true
  };

  this.elements = [];

  this.config = _.extend(defaults, this.options); // getting internal positions
  this.config.pageText = '';
  this.config.y = this.top();
  this.config.x = this.parentInstance.config.x;
  this.config = _.extend(this.config, this.getInnerXAndY()); // getting internal positions

  return this;
};
PDFToolkit.prototype.PageFooter.prototype.updateTotalPages = function () {
  var appendTotalPagesText = '';
  if (this.config.totalPages) {
    appendTotalPagesText = ' of ' + this.config.totalPages;
  }
  this.config.pageText = 'Page ' + this.parentInstance.countPages() + appendTotalPagesText; // we always render footer in the last page
  return this;
};
PDFToolkit.prototype.PageFooter.prototype.getInnerXAndY = function () {
  return {
    innerX: this.config.x + this.config.marginLeft,
    innerY: this.config.y + this.config.marginTop
  }
};
PDFToolkit.prototype.PageFooter.prototype.bottom = function () {
  return this.parentInstance.config.PDFInternalPoints.leftBottom;
};
PDFToolkit.prototype.PageFooter.prototype.render = function () {
  this.updateTotalPages();
  var marginBetweenColumns = 1,
    maxWidth = (this.footerWidth() / 2) - marginBetweenColumns,
    pageText,
    customText = new this.parentInstance.TextContent({
      pdf: this.parentInstance.pdf,
      parentInstance: this.parentInstance,
      text: this.config.customText,
      x: this.config.innerX,
      y: this.config.innerY,
      fontSize: this.config.fontSize,
      textColor: this.config.textColor,
      marginRight: marginBetweenColumns,
      breakPage: false, // never break on footer, it creates infinite loop
      maxWidth: maxWidth // no more than half of the footer
    }).render();

  if (this.config.pageCount) {
    pageText = new this.parentInstance.TextContent({
      pdf: this.parentInstance.pdf,
      parentInstance: this.parentInstance,
      text: this.config.pageText,
      horizontalAlign: 'right',
      x: this.config.innerX + customText.config.maxWidth,
      y: this.config.innerY,
      fontSize: this.config.fontSize,
      textColor: this.config.textColor,
      marginLeft: marginBetweenColumns,
      breakPage: false,
      maxWidth: maxWidth // no more than half of the footer
    }).render();
  }

  this.parentInstance.pdf.setDrawColor(this.config.textColor);
  // horizontal line
  this.parentInstance.pdf.line(this.config.x, this.config.y, this.config.x + this.footerWidth(), this.config.y);

  this.elements = [];
  this.elements.push(customText);
  this.elements.push(pageText);
  return this;
};
PDFToolkit.prototype.PageFooter.prototype.footerWidth = function () {
  return this.parentInstance.config.innerDimensions.width - (this.config.marginLeft + this.config.marginRight);
};
PDFToolkit.prototype.PageFooter.prototype.top = function () {
  var customText = new this.parentInstance.TextContent({
      pdf: this.parentInstance.pdf,
      parentInstance: this.parentInstance,
      text: this.config.customText,
      fontSize: this.config.fontSize,
      maxWidth: (this.footerWidth() / 2) // no more than half of the footer
    }),
    pageText = new this.parentInstance.TextContent({
      pdf: this.parentInstance.pdf,
      parentInstance: this.parentInstance,
      text: this.config.pageText,
      fontSize: this.config.fontSize,
      maxWidth: (this.footerWidth() / 2) // no more than half of the footer
    }),
    maxHeight = _.max([customText, pageText], function (element) {
      return element.height();
    }),
    totalFooterHeight = this.config.marginTop + maxHeight.height() + this.config.marginBottom;

  return this.parentInstance.config.PDFInternalPoints.leftBottom.y - totalFooterHeight;
};

//module.exports = PDFToolkit;