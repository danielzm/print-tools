$(document).ready(function(){
    $('.from-html').on('click', function(){
        function getHeader () {
            return $('header')[0];
        }
        function getTable () {
            return $('.print-forHTML')[0];
        }
        function getContent() {
            var wrapper = $('<div></div>')[0];
            wrapper.table = getTable();
            wrapper.header = getHeader();
            wrapper.showTable = function showTable() {
                $(this.table).show();
            };
            wrapper.hideTable = function hideTable() {
                $(this.table).hide();
            };

            $(wrapper.header).clone().appendTo(wrapper);
            wrapper.appendChild(wrapper.table);
            wrapper.showTable();
            return wrapper;
        }
        try{
            var doc = new jsPDF(),
                content = getContent();

            doc.fromHTML(content, 15, 15, {
                'width': 170
            }, function(msg){
                content.hideTable();
            });

            doc.save('PDFTool');
        } catch(e) {
            console.error(e.message,e.stack,e);
        };
    });

});